var Game = function(canvasElement)
{
	this.canvas = document.getElementById(canvasElement);
	this.context = this.canvas.getContext("2d");
	this.name;
	this.fps = 30;
	this.time = 0;
	this.frames = 0;
	this.count = 0;
	this.scene = "gameplay";
	this.count_games = 1;
	this.mouseon_position = {x:0,y:0};
	this.mouseclick_position = {x:0,y:0};
	this.blocks_data = [];
	this.player = [{"char":"X","score":0}, {"char":"O","score":0}];
	this.sum_score = true;
	this.current_player; // 0 = X | 1 = O
	this.winner = "";
	this.break_even = false;

	this.construct = function() {
		// Define um array com as posições de cada bloco
		this.blocks_data = [
			[	// Primeira linha de blocos
				{x:210,y:50,w:100,h:100,clicked:0,player:""},
				{x:316,y:50,w:100,h:100,clicked:0,player:""},
				{x:420,y:50,w:100,h:100,clicked:0,player:""}
			],				
			[	// Segunda linha de blocos
				{x:210,y:156,w:100,h:100,clicked:0,player:""},
				{x:316,y:156,w:100,h:100,clicked:0,player:""},
				{x:420,y:156,w:100,h:100,clicked:0,player:""}
			],				
			[	// Terceira linha de blocos
				{x:210,y:263,w:100,h:100,clicked:0,player:""},
				{x:316,y:263,w:100,h:100,clicked:0,player:""},
				{x:420,y:263,w:100,h:100,clicked:0,player:""}
			]
		];

		var random = Math.floor(Math.random()*2+1);
		if ( random == 2 ) {
			this.current_player = 1;
		} else {
			this.current_player = 0;				
		}		 
	};


	/**
	* gameLoop. Loop de repedição que roda na velocidade do FPS definido
	*/
	this.gameLoop = function() {
		this.countFrames();

		this.clearScreen();

		if ( this.scene == "gameplay" ) {
			this.drawGrid();
			this.drawInfo();
		}

		if ( this.scene == "winner" ) {
			if ( this.sum_score ) {
				var winner = (this.winner == "X") ? 0 : 1;
				this.player[winner].score ++;
				this.sum_score = false;
			}			

			this.drawDialog(this.winner+ " Venceu!");
		}

		if ( this.scene == "break_even" ) {
			this.drawDialog("Sem vencedor");
		}

		var game = this;
		this.canvas.onmousemove = function(e) {
			game.mouseon_position.x = e.clientX;
			game.mouseon_position.y = e.clientY;
		};

		this.canvas.addEventListener("mousedown",function(event){
			game.getMouseClick(event);
		}, false);
	};	


	/**
	* resetGame. Reinicia o jogo para uma nova partida.
	* Reinicia todos os valores usados na partida.
	*/
	this.resetGame = function()
	{
		if ( this.scene == "winner" || this.scene == "break_even" ) {
			this.count_games ++;
		}

		this.scene = "gameplay";
		this.mouseon_position = {x:0,y:0};
		this.mouseclick_position = {x:0,y:0};
		this.blocks_data = [];
		this.current_player; // 0 = X | 1 = O
		this.winner = "";
		this.break_even = false;
		this.sum_score = true;

		this.construct();
	}


	/**
	* clone. Clona o objeto.
	*/
	this.clone = function() {
	    return Object.create(this);
	};


	/**
	* changePlayer. Faz a troca de usuário a cada jogada
	*/
	this.changePlayer = function()
	{
		if ( this.current_player == 1 ) {
			this.current_player = 0;
		} else {
			this.current_player = 1;
		}
	}


	/**
	* drawRect. Desenha um retangulo.
	*
	* @param int posX Posição X
	* @param int posY Posição Y
	* @param int width Largura
	* @param int height Altura
	* @param String color Cor em rgba
	*/
	this.drawRect = function (posX, posY, width, height, color) {
		if ( color == undefined ) {
			color = "rgb(0,0,0)";
		}

		this.context.fillStyle = color;
		var bloco = this.context.fillRect(posX, posY, width, height);
		
	};


	/**
	* drawLine. Desenha linhas, de acordo com os valores recebidos
	*
	* @param vertices
	* [
	*	{moveTo:{x:100,y:100},lineTo:{x:100,y:100}},
	* 	{moveTo:{x:100,y:100},lineTo:{x:100,y:100}},
	* ]
	*/
	this.drawLine = function(vertices, width) {

		this.context.beginPath();
		this.context.lineWidth = width;
		for ( var item in vertices ) {
			this.context.moveTo(vertices[item].moveTo.x,vertices[item].moveTo.y);
			this.context.lineTo(vertices[item].lineTo.x, vertices[item].lineTo.y);
		}

		this.context.stroke();
		this.context.lineWidth = 1;
	};



	/**
	* drawPlayer. Desenha o player (X ou O) no lugar clicado
	*/
	this.drawPlayer = function(player, x,y)
	{
		this.context.beginPath();
		this.context.lineWidth = 5;
		
		if ( player == "X" ) {
			x = x+20;
			y = y+20;
			this.context.moveTo(x,y);
			this.context.lineTo((x+60),(y+60));
			this.context.moveTo((x+60),y);
			this.context.lineTo(x,y+60);
			this.context.strokeStyle = "rgb(255,0,0)";
		}

		if ( player == "O" ) {
			x = x+50;
			y = y+50;
			this.context.arc(x,y,40,0,2*Math.PI);
			this.context.strokeStyle = "rgb(0,0,255)";				
		}

		this.context.stroke();
		this.context.lineWidth = 1;
		this.context.strokeStyle = "rgb(0,0,0)";
	}



	/**
	* drawGrid. Desenha o grid do jogo da velha
	*/
	this.drawGrid = function()
	{
		// Desenha as linhas
		var lines = [
			// Linhas horizontais
			{moveTo:{x:200,y:153},lineTo:{x:540,y:153}},
			{moveTo:{x:200,y:260},lineTo:{x:540,y:260}},
			// Linhas verticais
			{moveTo:{x:313,y:10},lineTo:{x:313,y:400}},
			{moveTo:{x:418,y:10},lineTo:{x:418,y:400}},
		];
		this.drawLine(lines, 5);			

		// Desenha os blocos
		for ( var index in this.blocks_data ) {
			for (var i=0; i<this.blocks_data[index].length; i++ ) {		

				// Cor padrão o bloco (sem o mouse on)
				var color = "rgba(100,100,100,0.03)";

				// Se o mouse estiver sobre o bloco (mouse on) cria o bloco com um cor diferente
				if ( this.checkMouseOnBlock(this.mouseon_position, this.blocks_data[index][i]) ) {
					color ="rgba(255,100,100,0.3)";
				}

				// Verifica se o mouse click foi em cima do bloco
				if ( this.checkMouseOnBlock(this.mouseclick_position, this.blocks_data[index][i]) ) {
					// Se o bloco ainda não foi clicado antes, então marca como clicado
					if ( this.blocks_data[index][i].clicked == 0 ) {
						console.log(this.player[this.current_player].char);

						this.blocks_data[index][i].clicked = 1;
						this.blocks_data[index][i].player = this.player[this.current_player].char;
						
						var winner = this.checkWinner();

						if ( !winner ) {
							this.checkBreakEven();
						}

						this.changePlayer();

					}
				}


				if ( this.blocks_data[index][i].clicked == 1 ) {
					color ="rgba(100,100,100,0.3)";
					this.drawPlayer(
						this.blocks_data[index][i].player, 
						this.blocks_data[index][i].x,
						this.blocks_data[index][i].y
					);
				}

				// Desenha o bloco
				this.drawRect(
					this.blocks_data[index][i].x,
					this.blocks_data[index][i].y,
					100,
					100,
					color
				);
				
			}
		}
	};


	/**
	* drawDialog. Desenha a caixa de mensagem,
	* para mostrar o ganhador ou empate
	*/
	this.drawDialog = function(message)
	{
		var rectPosition = {
			x:250,
			y:100,
			w:300,
			h:300
		};

		// Define os dados do botão
		var btnPostion = {
			x:0,
			y:250,
			w:110,
			h:40
		};

		this.drawRect(
			rectPosition.x,
			rectPosition.y,
			rectPosition.w,
			rectPosition.h,
			"rgb(255,255,255)"
		);

		this.context.fillStyle="rgb(0,0,0)";
		this.context.font="25px verdana";
		this.context.fillText(message, 330, 200);

		// Cria um botão
		// Define a posição X do botão para o centro do retangulo "container"
		btnPostion.x = rectPosition.x+(rectPosition.w/2)-(btnPostion.w/2);

		var gradient = this.context.createLinearGradient(0,0,0,170);

		if ( this.checkMouseOnBlock(this.mouseclick_position, btnPostion) ) {
			this.resetGame();
		}

		if ( this.checkMouseOnBlock(this.mouseon_position, btnPostion) ) {
			gradient.addColorStop(1,"rgb(7,65,212)");
			gradient.addColorStop(0,"rgb(145,197,255)");
		} else {
			gradient.addColorStop(0,"rgb(7,65,212)");
			gradient.addColorStop(1,"rgb(145,197,255)");
		}

		this.drawRect(
			btnPostion.x,
			btnPostion.y,
			btnPostion.w,
			btnPostion.h,
			gradient
		);

		this.context.fillStyle="rgb(0,0,0)";
		this.context.font="14px verdana";
		this.context.fillText("nova partida", btnPostion.x+10, btnPostion.y+25);
	}


	/**
	* drawInfo Desenha a caixa de informações do jogo.
	* quantidades de partidas e pontuação dos jogadores.
	*/
	this.drawInfo = function()
	{
		this.drawRect(
			10,
			50,
			150,
			200,
			"rgb(255,255,255)"
		);

		this.context.fillStyle="rgb(0,0,0)";
		this.context.font="14px verdana";

		this.context.fillText("Partida: "+this.count_games, 20, 70);
		this.context.fillText("Pontuacao", 20, 100);
		this.context.fillText("X: "+this.player[0].score, 20, 120);
		this.context.fillText("O: "+this.player[1].score, 20, 140);
	}



	/**
	* checkWinner. Verifica se algum player venceu a partida
	* comparando as combinações do jogo da velha.
	* na horizontal: X X X
	*
	* na vertical: X
	*              X
	*              X
	* 
	* na diagonal: X
	*	            X
	*                X
	*/
	this.checkWinner = function()
	{
		var win = false;
		for ( var i=0; i<3; i++ ) {
			if ( !win ) {
				if ( (this.blocks_data[i][0].player == "X" && this.blocks_data[i][1].player == "X" && this.blocks_data[i][2].player == "X") ||

				 (this.blocks_data[0][i].player == "X" && this.blocks_data[1][i].player == "X" && this.blocks_data[2][i].player == "X") ||

				 (this.blocks_data[0][0].player == "X" && this.blocks_data[1][1].player == "X" && this.blocks_data[2][2].player == "X") ||

				 (this.blocks_data[0][2].player == "X" && this.blocks_data[1][1].player == "X" && this.blocks_data[2][0].player == "X") ) {

				 	this.winner = "X";
					console.log(this.winner + " WIN");
					win = true;
					this.scene = "winner";
				}

				if ( (this.blocks_data[i][0].player == "O" && this.blocks_data[i][1].player == "O" && this.blocks_data[i][2].player == "O") ||

				 (this.blocks_data[0][i].player == "O" && this.blocks_data[1][i].player == "O" && this.blocks_data[2][i].player == "O") ||

				 (this.blocks_data[0][0].player == "O" && this.blocks_data[1][1].player == "O" && this.blocks_data[2][2].player == "O") ||

				 (this.blocks_data[0][2].player == "O" && this.blocks_data[1][1].player == "O" && this.blocks_data[2][0].player == "O") ) {

				 	this.winner = "O";
					console.log(this.winner +" WIN");
					win = true;
					this.scene = "winner";
				}
			}
		}

		
		return win;
	};


	/**
	* checkBreakEven. Verifica se a partida terminou em empate.
	*/
	this.checkBreakEven = function()
	{
		var break_even = true;
		for (var i = 0; i < this.blocks_data.length; i++) {
			for (var j = 0; j < this.blocks_data[i].length; j++) {
				if ( this.blocks_data[i][j].clicked == 0 ) {
					break_even = false;
				}
			}
		}

		if ( break_even ) {
			this.break_even = true;
			this.scene = "break_even";			
		}
	};


	/**
	* checkMouseOnBlock. Verifica se o ponteiro do mouse esta encima de do bloco
	*/
	this.checkMouseOnBlock = function(mouse_position, block_position)
	{
		// Condição para o posição X do mouse
		var mouseX = mouse_position.x;
		var mouseY = mouse_position.y;
		var blocoX = block_position.x;
		var blocoY = block_position.y;
		var blocoW = parseInt(block_position.x) + block_position.w;
		var blocoH = parseInt(block_position.y) + block_position.h;

		var mouseOnX = ((mouseX >= blocoX) && (mouseX <= (parseInt(blocoX)+block_position.w)));
		var mouseOnY = ((mouseY >= blocoY) && (mouseY <= (parseInt(blocoY)+block_position.h)));

		if ( mouseOnX && mouseOnY ) {
			return true;
		} else {
			return false;
		}
	};


	/**
	* countFrames. Conta os frames e o tempo de execução do game
	*/
	this.countFrames = function()
	{
		this.frames ++;
		if (this.frames % this.fps == 0) {
			this.time ++;
		}
	}
	

	/**
	* getMouseClick. Pega o a posição do mouse quando clicado
	*/
	this.getMouseClick = function(event){
		this.mouseclick_position.x = event.clientX;
		this.mouseclick_position.y = event.clientY;
	}
	

	/**
	* clearScreen. Limpa a tela do canvas
	*/
	this.clearScreen = function() {
		this.context.clearRect(0, 0, 800, 600);
	}


	// Executa o metodo construtor
	this.construct();
};